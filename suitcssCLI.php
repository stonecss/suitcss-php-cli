<?php

/**
 * @file
 * Contains 'suitcssCLI' class; an abstraction layer for command line 'suitcss'
 */
class suitcssCLI {
  // Base command is hardcoded here to reduce security vulnerability.
  private static $base_command = 'suitcss';

  protected $input_file = NULL;

  // Holds value for current error.
  protected $error = NULL;

  /**
   * Constructor for Suitcss
   *
   * @param string $input_file
   *   Path for .css file relative to getcwd().
   */
  public function __construct($input_file = NULL) {
    $this->input_file = $input_file;
  }

  /**
   * Returns the version string from command line 'suitcss'.
   *
   * @return string
   *   Version string from 'suitcss'.
   */
  public function version() {
    return $this->proc_open(array('--version'));
  }

  /**
   * Returns the current error if one exists.
   *
   * @return string
   *   Error string returned by command line 'suitcss'.
   */
  public function get_error() {
    return $this->error;
  }

  /**
   * Executes preprocessing of CSS input.
   *
   * @return string
   *   Compiled CSS.
   */
  public function compile() {
    $command_arguments = array(
      $this->input_file,
    );

    return $this->proc_open($command_arguments);
  }

  private function proc_open($command_arguments = array(), $stdin = NULL) {
    $output_data = NULL;

    $command = implode(' ', array_merge(array(self::$base_command), $command_arguments));

    // Handles for data exchange.
    $pipes = array(
      0 => NULL, // STDIN
      1 => NULL, // STDOUT
      2 => NULL, // STDERR
    );

    // Sets permissions on $pipes.
    $descriptors = array(
      0 => array('pipe', 'r'), // STDIN
      1 => array('pipe', 'w'), // STDOUT
      2 => array('pipe', 'w'), // STDERR
    );

    $process = proc_open($command, $descriptors, $pipes);

    if (is_resource($process)) {
      $output_data = stream_get_contents($pipes[1]);
      fclose($pipes[1]);

      $this->error = stream_get_contents($pipes[2]);
      fclose($pipes[2]);

      proc_close($process);
    }

    return $output_data;
  }
}
